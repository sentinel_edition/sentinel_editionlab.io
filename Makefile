##
# sentinel_edition.gitlab.io
#
# @file
# @version 0.1

build:
	emacs --batch --no-init-file --load publish.el --funcall org-publish-all

# end
